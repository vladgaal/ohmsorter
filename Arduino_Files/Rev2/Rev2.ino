#define RANGE 10 // in % 
#define RA 470   // in ohm
#define RB 220   // in ohm
#define RC 47    // in ohm
#define RD 33    // in ohm

int analogPin = 0;
int Vin = 5;     // in V
float Vout = 0;
float R1 = 915;  // in ohm
float R2 = 0;
float buffer = 0;
int measure = 0;

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

void loop() {
  for (int i = 0; i < 10; i++) {
    measure = measure + analogRead(analogPin);
    delay(10);
  }

  buffer = measure * Vin / 10;
  Vout = (buffer) / 1024.0;
  buffer = (Vin / Vout) - 1;
  R2 = R1 * buffer;

  measure = 0;

  if (R2 > (RA - (RA * RANGE / 100)) && R2 < (RA + (RA * RANGE / 100))) digitalWrite(2, HIGH);
  else digitalWrite(2, LOW);
  if (R2 > (RB - (RB * RANGE / 100)) && R2 < (RB + (RB * RANGE / 100))) digitalWrite(3, HIGH);
  else digitalWrite(3, LOW);
  if (R2 > (RC - (RC * RANGE / 100)) && R2 < (RC + (RC * RANGE / 100))) digitalWrite(4, HIGH);
  else digitalWrite(4, LOW);
  if (R2 > (RD - (RD * RANGE / 100)) && R2 < (RD + (RD * RANGE / 100))) digitalWrite(5, HIGH);
  else digitalWrite(5, LOW);
}

# OhmSorter
Device to assist in sorting resistors

---

## 💻 Requirements

Before starting, make sure you have met the following requirements:

> Arduino IDE
> 
> Arduino NANO (or another model)
> 
> Some LEDs, 560 Ohm and 1 kOhm resistors 

---

## 🚀 Running OhmSorter

> 1 - Clone this repository into your computer.
> 
> 2 - Change the RA, RB, RC and RD values as desired.
> 
> 3 - Using the Arduino IDE upload the code to an arduino.
> 
> 4 - Assemble the circuit:
> 
> ![Alt text](./Pics/schematic.png "schematic"){height=50px}
>
> 5 - Place the resistor to be measured at the measuring points:
> 
> ![Alt text](./Pics/fig2a.png "measuring points"){height=50px}
>
> 6 - Place the resistor in the box according to the color of the LED:
>
> ![Alt text](./Pics/fig1a.png "Work flow"){height=50px}
>

---

## ✔️ Release control

Rev1 -> Initial release, just simple ohmmeter;

Rev2 -> Sort resistors in 4 categories;

---

## 📝 License

All assets and code are under the MIT LICENSE and in the public domain unless specified otherwise. Read the file [LICENSE](LICENSE.md) for more details.
